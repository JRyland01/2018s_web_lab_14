<%--
  Created by IntelliJ IDEA.
  User: Andrew Meads
  Date: 13/05/2018
  Time: 3:53 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Web Lab 17</title>
</head>
<body>
<ul>
    <li><a href="ImageGalleryDisplay">Image Gallery</a></li>
    <li><a href="exercise01-01.jsp">Exercise01.1</a></li>
    <li><a href="exercise01-02.jsp">Exercise01.2</a></li>
    <li><a href="exercise02-01.jsp">Exercise02.1</a></li>
    <li><a href="exercise02-02.jsp">Exercise02.2</a></li>
    <li><a href="ex08/edmund_hillary.html">Edmund Hillary</a></li>
    <li><a href="ex05/survey.html">Survey</a></li>
</ul>
</body>
</html>
