<%@ page import="java.io.File" %>
<%@ page import="java.util.List" %>
<%@ page import="ictgradschool.ictgradschool.web.lab14.imagegallery.ImageGalleryDisplay" %><%--
  Created by IntelliJ IDEA.
  User: jryl559
  Date: 10/01/2019
  Time: 12:24 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="C" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Image Gallery UI</title>
</head>
<body>
<p>PathName: $${photospath}</p>
<p>FileDataList Size: ${fileDataList.size()}</p>
<table>
    <tr>
        <th>Thumbnail</th>
        <th><a href="/ImageGalleryDisplay?sortColumn=filename&order=${filenameSortToggle}ending"><img src="images/sort-${filenameSortToggle}.png"></a>File Name</th>
        <th><a href="/ImageGalleryDisplay?sortColumn=filesize&order=${filesizeSortToggle}ending"><img src="images/sort-${filesizeSortToggle}.png"></a>File size</th>
    </tr>
<c:forEach items="${fileDataList}" var="filerecord">
    <tr>
        <td>
            <a href='Photos/${filerecord.fullFile.getName()}'>
                <img src='Photos/${filerecord.thumbPath.name}'></a>
        </td>
        <td>
            <p>${filerecord.thumbDisplay}</p>
        </td>
        <td>
            <p> ${filerecord.fullfileSize}</p>
        </td>
    </tr>
    </c:forEach>
</table>
<%--<%--%>
    <%--for(int i = 0; i < fileDataList.size(); i++) {----%>
<%--FileInfo fileData = fileDataList.get(i);--%>
<%--File thumbNailFile = fileData.thumbPath;--%>
<%--if (thumbNailFile == null) continue;--%>
<%--File fullFile = fileData.fullFile;--%>
<%--long filesize = fileData.fullfileSize;--%>
<%--String displayStr = fileData.thumbDisplay--%>
        <%--%>--%>
</body>
</html>
